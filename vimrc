" bundles
" call pathogen#infect()
call pathogen#runtime_append_all_bundles() 
call pathogen#helptags()

syntax on
filetype plugin on
filetype plugin indent on

colorscheme elflord

set cursorline
set noic
set softtabstop=4
set expandtab
set nowrap        " don't wrap lines
set tabstop=4     " a tab is four spaces
set backspace=indent,eol,start
                  " allow backspacing over everything in insert mode
set autoindent    " always set autoindenting on
set copyindent    " copy the previous indentation on autoindenting
set number        " always show line numbers
set shiftwidth=4  " number of spaces to use for autoindenting
set shiftround    " use multiple of shiftwidth when indenting with '<' and '>'
set showmatch     " set show matching parenthesis
set ignorecase    " ignore case when searching
set smartcase     " ignore case if search pattern is all lowercase,
                  "    case-sensitive otherwise
set hlsearch      " highlight search terms
set incsearch     " show search matches as you type
" set nobackup
" Turn off annoying swapfiles
set noswapfile
set pastetoggle=<F2>
set nocompatible

set hidden

" Set persistent undo (v7.3 only)
set undodir=~/.vim/undodir
set undofile

" The PC is fast enough, do syntax highlight syncing from start
autocmd BufEnter * :syntax sync fromstart

" Activate a permanent ruler 
set ruler
  
" clear search highlight
nnoremap <leader><space> :noh<cr>

" clipboard
set clipboard=unnamed

filetype on
filetype plugin on

autocmd BufWritePost *.py call Flake8()
let g:flake8_ignore="E501,W293"

" nmap <F8> :TagbarToggle<CR> 
let g:tagbar_usearrows = 1
nnoremap <leader>l :TagbarToggle<CR>
  
" clear ^M messup
noremap <Leader>m mmHmt:%s/<C-V><cr>//ge<cr>'tzt'm

" Tab configuration
map <leader>tn :tabnew<cr>
map <leader>te :tabedit
map <leader>tc :tabclose<cr>
map <leader>tm :tabmove

" minibuf
let g:miniBufExplMapWindowNavVim = 1 
let g:miniBufExplMapWindowNavArrows = 1 
let g:miniBufExplMapCTabSwitchBufs = 1 
" let g:miniBufExplModSelTarget = 1 
let g:miniBufExplUseSingleClick = 1
map <Leader>u :UMiniBufExplorer<cr>

" MiniBufExpl Colors
hi MBEVisibleActive guifg=#A6DB29 guibg=fg
hi MBEVisibleChangedActive guifg=#F1266F guibg=fg
hi MBEVisibleChanged guifg=#F1266F guibg=fg
hi MBEVisibleNormal guifg=#5DC2D6 guibg=fg
hi MBEChanged guifg=#CD5907 guibg=fg
hi MBENormal guifg=#808080 guibg=fg

" Command-T
let g:CommandTMaxHeight = 15
set wildignore+=*.o,*.obj,.git,*.pyc,*.egg-info
noremap <leader>j :CommandT<cr>
noremap <leader>y :CommandTFlush<cr>
